.MODEL SMALL
.CODE
ORG 100h
MULAI :
	jmp CETAK
	Hello 	DB 'Selamat Datang'
		DB 'Di bahasa Assembly'
		DB '$'
CETAK :
	MOV AH,09H
	MOV DX,OFFSET Hello
	INT 21H
HABIS :
	INT 20h
END MULAI